import { Box, Container, Divider, Grid, Typography } from "@mui/material";
import { useEffect, useMemo, useRef, useState } from "react";
import { MainTournamentLayout } from "../../../components/main-tournament-layout";
import TournamentData from "../../../guards/TournamentData";
import useTournament from "../../../hooks/useTournament";
import dynamic from "next/dynamic";
import { QuillEditor } from "../../../components/quill-editor";

const RulesPage = () => {
  const { tournament } = useTournament();
  const [isLoading, setIsLoading] = useState(true);
  const quillRef = useRef();

  const ReactQuill = useMemo(() => dynamic(import("react-quill"), { ssr: false }), [tournament]);

  return (
    <Container
      sx={{
        mt: 5,
        mb: 5,
        display: "flex",
        flexDirection: "column",
      }}
    >
      <Box
        sx={{
          backgroundColor: "background.default",
          py: 6,
        }}
      >
        <Grid alignItems="center" container>
          <Grid item md={6} xs={12}>
            <Typography variant="h1">Rules</Typography>
            <Typography color="textSecondary" sx={{ mt: 1 }} variant="body1">
              {tournament.name}
            </Typography>
          </Grid>
        </Grid>
      </Box>
      <Divider />
      <Box
        sx={{
          backgroundColor: "background.default",
          minHeight: "100%",
          pt: 3,
        }}
      >
        <ReactQuill ref={quillRef.current} value={tournament.rules} readOnly theme={"bubble"} />
      </Box>
    </Container>
  );
};

RulesPage.getLayout = (page) => (
  <TournamentData>
    <MainTournamentLayout>{page}</MainTournamentLayout>
  </TournamentData>
);

export default RulesPage;
