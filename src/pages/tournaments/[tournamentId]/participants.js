import { Box, Container, Divider, Grid, Typography } from "@mui/material";
import { useState } from "react";
import { MainTournamentLayout } from "../../../components/main-tournament-layout";
import TournamentData from "../../../guards/TournamentData";
import useTournament from "../../../hooks/useTournament";
import ParticipantCard from "../../../components/participant-card";
import rca from "rainbow-colors-array";
import { getUserStatistics } from "../../../utils/user-utils";

const TournamentStaffPage = () => {
  const { tournament } = useTournament();
  const [isLoading, setIsLoading] = useState(true);

  const participantsCopy = [...tournament.participants];

  const participantsSortedByPp = participantsCopy.sort((a, b) => {
    const aStats = getUserStatistics(a.user, tournament.gameMode);
    const bStats = getUserStatistics(b.user, tournament.gameMode);
    return bStats.pp - aStats.pp;
  });

  const rainbowColors = rca(participantsSortedByPp.length, "hex");

  return (
    <Container
      sx={{
        mt: 5,
        mb: 5,
        display: "flex",
        flexDirection: "column",
      }}
    >
      <Box
        sx={{
          backgroundColor: "background.default",
          py: 6,
        }}
      >
        <Grid alignItems="center" container>
          <Grid item md={6} xs={12}>
            <Typography variant="h1">Participants</Typography>
            <Typography color="textSecondary" sx={{ mt: 1 }} variant="body1">
              {tournament.name}
            </Typography>
          </Grid>
        </Grid>
      </Box>
      <Divider />
      <Box
        sx={{
          backgroundColor: "background.default",
          minHeight: "100%",
          pt: 3,
        }}
      >
        <Grid container spacing={2}>
          {participantsSortedByPp.map((participant, index) => (
            <Grid item key={participant.id} lg={3} md={3} sm={6} xs={12}>
              <ParticipantCard
                tournament={tournament}
                participant={participant}
                index={index + 1}
                hex={rainbowColors[index]}
              />
            </Grid>
          ))}
        </Grid>
      </Box>
    </Container>
  );
};

TournamentStaffPage.getLayout = (page) => (
  <TournamentData>
    <MainTournamentLayout>{page}</MainTournamentLayout>
  </TournamentData>
);

export default TournamentStaffPage;
