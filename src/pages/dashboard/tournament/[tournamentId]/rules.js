import TournamentData from "../../../../guards/TournamentData";
import TournamentGuard from "../../../../guards/TournamentGuard";
import { DashboardLayout } from "../../../../components/dashboard-layout";
import Head from "next/head";
import { Box, Button, Container } from "@mui/material";
import useTournament from "../../../../hooks/useTournament";
import { useState } from "react";
import dynamic from "next/dynamic";
import { QuillEditor } from "../../../../components/quill-editor";
import { rulesApi } from "../../../../api/rulesApi";
import toast from "react-hot-toast";
import { useDispatch } from "react-redux";
import { setTournamentRules } from "../../../../slices/tournament";

const ReactQuill = dynamic(() => import("react-quill"), { ssr: false });
// const QuillEditor = dynamic(() => import("../../../../components/quill-editor"), {
//   ssr: false,
// });

const RulesPage = () => {
  const { tournament } = useTournament();
  const [rules, setRules] = useState(tournament.rules);
  const dispatch = useDispatch();

  const handleRulesChange = (value) => {
    setRules(value);
  };

  const handleRulesSave = () => {
    const body = {
      rules,
    };
    rulesApi.changeRules(tournament.id, body).then((response) => {
      dispatch(setTournamentRules(rules));
      toast.success(response.data);
    });
  };

  return (
    <>
      <Head>
        <title>Rules | {tournament.name}</title>
      </Head>
      <Box
        component="main"
        sx={{
          flexGrow: 1,
          py: 4,
        }}
      >
        <Container maxWidth={false}>
          <Button onClick={handleRulesSave}>Save rules</Button>
          <QuillEditor
            onChange={handleRulesChange}
            placeholder="Write something"
            sx={{ height: 400 }}
            value={rules}
          />
          <ReactQuill value={rules} readOnly={true} theme={"bubble"} />
        </Container>
      </Box>
    </>
  );
};

RulesPage.getLayout = (page) => (
  <TournamentData>
    <TournamentGuard>
      <DashboardLayout>{page}</DashboardLayout>
    </TournamentGuard>
  </TournamentData>
);

export default RulesPage;
