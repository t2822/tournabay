import PropTypes from "prop-types";
import {
  Avatar,
  Box,
  Card,
  CardContent,
  CardMedia,
  Divider,
  Grid,
  TextField,
  Typography,
} from "@mui/material";
import { useState } from "react";
import toast from "react-hot-toast";
import { useDispatch } from "react-redux";
import useTournament from "../hooks/useTournament";
import { teamApi } from "../api/teamApi";
import axios from "axios";
import { ACCESS_TOKEN, API_URL } from "../constants/constants";
import { FileDropzone } from "./file-dropzone";

const TeamManagementDialog = (props) => {
  const { tournament } = useTournament();
  const { team, closeModal } = props;
  const [logoFile, setLogoFile] = useState([]);
  const [logoFileUrl, setLogoFileUrl] = useState(
    `https://tournabay.s3.amazonaws.com/tournament/${tournament.id}/team/${team.id}/logo.jpg`
  );
  const [tempLogoFileUrl, setTempLogoFileUrl] = useState(null);

  const [bannerFile, setBannerFile] = useState([]);
  const [bannerFileUrl, setBannerFileUrl] = useState(
    `https://tournabay.s3.amazonaws.com/tournament/${tournament.id}/team/${team.id}/banner.jpg`
  );
  const [tempBannerFileUrl, setTempBannerFileUrl] = useState(null);
  const [requestLoading, setRequestLoading] = useState(false);
  const dispatch = useDispatch();

  const handleLogoDrop = (newFiles) => {
    setLogoFile([...newFiles]);
    setTempLogoFileUrl(URL.createObjectURL(newFiles[0]));
  };

  const handleLogoRemove = (file) => {
    setLogoFile([]);
  };

  const handleLogoRemoveAll = () => {
    setLogoFile([]);
  };

  const handleBannerDrop = (newFiles) => {
    setBannerFile([...newFiles]);
    setTempBannerFileUrl(URL.createObjectURL(newFiles[0]));
  };

  const handleBannerRemove = (file) => {
    setBannerFile([]);
  };

  const handleBannerRemoveAll = () => {
    setBannerFile([]);
  };

  const onLogoUpload = () => {
    const toastLoadingId = toast.loading("Uploading logo");
    let formData = new FormData();

    formData.append("file", logoFile[0]);

    axios
      .put(
        `${API_URL}/s3/upload/logo/tournament/${tournament.id}/team/${team.id}?fileType=LOGO`,
        formData,
        {
          headers: {
            "Content-Type": "multipart/form-data",
            Authorization: "Bearer " + localStorage.getItem(ACCESS_TOKEN),
          },
        }
      )
      .then((response) => {
        setTempLogoFileUrl(null);
        setLogoFile([]);
        toast.success("Logo uploaded!");
      })
      .catch((error) => console.error(error))
      .finally(() => toast.remove(toastLoadingId));
  };

  const onBannerUpload = () => {
    const toastLoadingId = toast.loading("Uploading banner");
    let formData = new FormData();

    formData.append("file", bannerFile[0]);

    axios
      .put(
        `${API_URL}/s3/upload/logo/tournament/${tournament.id}/team/${team.id}?fileType=BANNER`,
        formData,
        {
          headers: {
            "Content-Type": "multipart/form-data",
            Authorization: "Bearer " + localStorage.getItem(ACCESS_TOKEN),
          },
        }
      )
      .then((response) => {
        setTempBannerFileUrl(null);
        setBannerFile([]);
        toast.success("Banner uploaded!");
      })
      .catch((error) => console.error(error))
      .finally(() => toast.remove(toastLoadingId));
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    const toastLoadingId = toast.loading("Adding team");
    if (!captain) {
      toast.remove(toastLoadingId);
      toast.error("Captain cannot be empty!");
      return;
    }
    setRequestLoading(true);
    const body = {
      name: team.name,
    };
    teamApi
      .createTeam(tournament.id, body)
      .then((res) => {
        toast.success("Team created successfully");
        dispatch(addTeam(res.data));
        closeModal();
      })
      .catch((err) => {
        const errors = err.response.data.errors ?? undefined;
        if (errors) {
          toast.error(errors[0].defaultMessage);
        } else {
          toast.error(err.response.data.message);
        }
      })
      .finally(() => {
        toast.remove(toastLoadingId);
        setRequestLoading(false);
      });
  };

  return (
    <form>
      <Card>
        <CardMedia
          image={tempBannerFileUrl ? tempBannerFileUrl : `${bannerFileUrl}?${Date.now()}`}
          sx={{ height: 80 }}
        />
        <CardContent sx={{ p: 0 }}>
          <Box
            sx={{
              display: "flex",
              justifyContent: "center",
              mb: 2,
              mt: "-50px",
            }}
          >
            <Avatar
              alt="Applicant"
              src={tempLogoFileUrl ? tempLogoFileUrl : `${logoFileUrl}?${Date.now()}`}
              sx={{
                border: "3px solid #FFFFFF",
                height: 100,
                width: 100,
              }}
            />
          </Box>
          <Box sx={{ pb: 3, display: "flex", justifyContent: "center" }}>
            <Typography color="textPrimary" gutterBottom variant="h4">
              {team.name}
            </Typography>
          </Box>
          <Divider />
          <Box sx={{ p: 3 }}>
            <Typography color="textPrimary" gutterBottom variant="subtitle1">
              Roster
            </Typography>
            {team.participants.map((participant) => (
              <Box key={participant.id} sx={{ display: "flex", alignItems: "center", my: 1 }}>
                <Avatar
                  alt={participant.user.username}
                  src={participant.user.avatarUrl}
                  sx={{
                    height: 30,
                    width: 30,
                  }}
                />
                <Typography align="center" variant="body2" sx={{ ml: 1 }}>
                  {participant.user.username}
                </Typography>
              </Box>
            ))}
          </Box>
          <Divider />
          <Box sx={{ p: 3 }}>
            <Grid container spacing={1}>
              <Grid item md={3} xs={12}>
                <Typography variant="h6">Team logo</Typography>
              </Grid>
              <Grid item md={9} xs={12}>
                <FileDropzone
                  accept="image/jpeg, image/png, image/jpg"
                  maxFiles={1}
                  files={logoFile}
                  onDrop={handleLogoDrop}
                  onRemove={handleLogoRemove}
                  onRemoveAll={handleLogoRemoveAll}
                  onUpload={onLogoUpload}
                  // in B
                  // max 5MB
                  maxSize={5000000}
                />
              </Grid>
            </Grid>
          </Box>
          <Box sx={{ p: 3 }}>
            <Grid container spacing={3}>
              <Grid item md={3} xs={12}>
                <Typography variant="h6">Team banner</Typography>
              </Grid>
              <Grid item md={9} xs={12}>
                <FileDropzone
                  accept="image/jpeg, image/png, image/jpg"
                  maxFiles={1}
                  files={bannerFile}
                  onDrop={handleBannerDrop}
                  onRemove={handleBannerRemove}
                  onRemoveAll={handleBannerRemoveAll}
                  onUpload={onBannerUpload}
                  // in B
                  // max 5MB
                  maxSize={5000000}
                />
              </Grid>
            </Grid>
          </Box>
        </CardContent>
      </Card>
    </form>
  );
};

TeamManagementDialog.propTypes = {
  closeModal: PropTypes.func,
};

export default TeamManagementDialog;
