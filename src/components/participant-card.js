import {
  Avatar,
  Box,
  Card,
  CardContent,
  CardMedia,
  Divider,
  Link,
  Typography,
} from "@mui/material";
import { getUserStatistics } from "../utils/user-utils";

const ParticipantCard = (props) => {
  const { participant, index, hex, tournament } = props;
  console.log(participant);
  const statistics = getUserStatistics(participant.user, tournament.gameMode);
  return (
    <Card>
      <CardMedia
        image={
          participant.user.coverUrl || "https://osu.ppy.sh/images/headers/profile-covers/c3.jpg"
        }
        sx={{ height: 80 }}
      />
      <CardContent sx={{ pt: 0 }}>
        <Box
          sx={{
            display: "flex",
            justifyContent: "center",
            mb: 2,
            mt: "-50px",
          }}
        >
          <Avatar
            alt="Applicant"
            src={participant.user.avatarUrl}
            sx={{
              border: "3px solid #FFFFFF",
              height: 100,
              width: 100,
            }}
          />
        </Box>
        <Box sx={{ display: "flex", alignItems: "center", justifyContent: "center" }}>
          <Link
            href={`https://osu.ppy.sh/users/${participant.user.osuId}`}
            align="center"
            color="textPrimary"
            sx={{ display: "block", mr: 1 }}
            underline="none"
            variant="h6"
            target="_blank"
          >
            {participant.user.username}
          </Link>
          <img
            style={{ height: 25, width: 25, borderRadius: 12 }}
            alt={participant.user.countryCode}
            src={`https://purecatamphetamine.github.io/country-flag-icons/3x2/${participant.user.countryCode}.svg`}
          />
        </Box>
        <Divider sx={{ my: 2 }} />
        <Typography align="center" variant="h6" sx={{ color: `#${hex.hex}` }}>
          {index}
          <Typography variant="subtitle2" color="textSecondary">
            {statistics.pp}pp
          </Typography>
        </Typography>
        <Divider sx={{ my: 2 }} />
        <Box sx={{ display: "flex", justifyContent: "center" }}>{participant.discordTag}</Box>
      </CardContent>
    </Card>
  );
};

export default ParticipantCard;
