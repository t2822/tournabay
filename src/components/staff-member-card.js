import { Avatar, Box, Card, CardContent, CardMedia, Link } from "@mui/material";

const StaffMemberCard = (props) => {
  const { staffMember } = props;

  return (
    <Card>
      <CardMedia
        image={
          staffMember.user.coverUrl || "https://osu.ppy.sh/images/headers/profile-covers/c3.jpg"
        }
        sx={{ height: 100 }}
      />
      <CardContent sx={{ pt: 0 }}>
        <Box
          sx={{
            display: "flex",
            justifyContent: "center",
            mb: 2,
            mt: "-50px",
          }}
        >
          <Avatar
            alt="Applicant"
            src={staffMember.user.avatarUrl}
            sx={{
              border: "3px solid #FFFFFF",
              height: 100,
              width: 100,
            }}
          />
        </Box>
        <Link
          href={`https://osu.ppy.sh/users/${staffMember.user.osuId}`}
          align="center"
          color="textPrimary"
          sx={{ display: "block" }}
          underline="none"
          variant="h6"
          target="_blank"
        >
          {staffMember.user.username}
        </Link>
        {/*<Typography align="center" variant="body2" color="textSecondary">*/}
        {/*  {applicant.commonConnections} contacts in common*/}
        {/*</Typography>*/}
        {/*<Divider sx={{ my: 2 }} />*/}
        {/*<Box sx={{ m: -0.5 }}>*/}
        {/*  {applicant.skills.map((skill) => (*/}
        {/*    <Chip key={skill} label={skill} sx={{ m: 0.5 }} variant="outlined" />*/}
        {/*  ))}*/}
        {/*</Box>*/}
      </CardContent>
    </Card>
  );
};

export default StaffMemberCard;
