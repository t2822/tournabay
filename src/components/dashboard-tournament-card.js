import { formatDistanceToNowStrict } from "date-fns";
import {
  Avatar,
  Box,
  Card,
  CardContent,
  CardMedia,
  Chip,
  Divider,
  Link,
  Typography,
} from "@mui/material";

const DashboardTournamentCard = (props) => {
  const { tournament, showRoles, cardMediaHeight = 60, tournamentLogoHeight = 80 } = props;

  return (
    <Card sx={{ width: "100%", boxShadow: "none" }}>
      <CardMedia
        image={`https://tournabay.s3.amazonaws.com/tournament/${tournament.id}/banner.jpg`}
        sx={{ height: cardMediaHeight, width: "auto" }}
      />
      <CardContent sx={{ p: "5px !important" }}>
        <Box
          sx={{
            display: "flex",
            justifyContent: "center",
            mb: 2,
            mt: "-50px",
          }}
        >
          <Avatar
            alt="Applicant"
            src={`https://tournabay.s3.amazonaws.com/tournament/${tournament.id}/logo.jpg`}
            sx={{
              border: "3px solid #FFFFFF",
              height: tournamentLogoHeight,
              width: tournamentLogoHeight,
            }}
          />
        </Box>
        <Link
          align="center"
          color="textPrimary"
          sx={{ display: "block" }}
          underline="none"
          variant="h6"
        >
          {tournament.name}
        </Link>
        <Typography align="center" variant="body2" color="textSecondary">
          {tournament.initials}
        </Typography>
        {showRoles && (
          <>
            <Divider sx={{ my: 2 }} />
            <Box sx={{ m: -0.5 }}>asd</Box>
          </>
        )}
      </CardContent>
    </Card>
  );
};

export default DashboardTournamentCard;
