import { Avatar, Box, Card, CardContent, CardMedia, Divider, Typography } from "@mui/material";

const TeamCard = (props) => {
  const { team, tournament } = props;

  return (
    <Card>
      <CardMedia
        image={`https://tournabay.s3.amazonaws.com/tournament/${tournament.id}/team/${team.id}/banner.jpg`}
        sx={{ height: 80 }}
      />
      <CardContent sx={{ pt: 0 }}>
        <Box
          sx={{
            display: "flex",
            justifyContent: "center",
            mb: 2,
            mt: "-50px",
          }}
        >
          <Avatar
            alt="Applicant"
            src={`https://tournabay.s3.amazonaws.com/tournament/${tournament.id}/team/${team.id}/logo.jpg`}
            sx={{
              border: "3px solid #FFFFFF",
              height: 100,
              width: 100,
            }}
          />
        </Box>
        <Typography align="center" variant="h6">
          {team.name}
        </Typography>
        <Divider sx={{ my: 2 }} />
        {team.participants.map((participant) => (
          <Box key={participant.id} sx={{ display: "flex", alignItems: "center", my: 1 }}>
            <Avatar
              alt={participant.user.username}
              src={participant.user.avatarUrl}
              sx={{
                height: 30,
                width: 30,
              }}
            />
            <Typography align="center" variant="body2" sx={{ ml: 1 }}>
              {participant.user.username}
            </Typography>
          </Box>
        ))}
      </CardContent>
    </Card>
  );
};

export default TeamCard;
