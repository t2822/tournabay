import { useState } from "react";
import PropTypes from "prop-types";
import { styled } from "@mui/material/styles";
import { Footer } from "./footer";
import { MainTournamentNavbar } from "./main-tournament-navbar";
import { MainTournamentSidebar } from "./main-tournament-sidebar";

const MainLayoutRoot = styled("div")(({ theme }) => ({
  backgroundColor: theme.palette.background.default,
  height: "100%",
  paddingTop: 64,
}));

export const MainTournamentLayout = ({ children }) => {
  const [isSidebarOpen, setIsSidebarOpen] = useState(false);

  return (
    <MainLayoutRoot>
      <MainTournamentNavbar onOpenSidebar={() => setIsSidebarOpen(true)} />
      <MainTournamentSidebar onClose={() => setIsSidebarOpen(false)} open={isSidebarOpen} />
      {children}
      <Footer />
    </MainLayoutRoot>
  );
};

MainTournamentLayout.propTypes = {
  children: PropTypes.node,
};
