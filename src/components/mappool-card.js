import { Box, Card, CardContent, CardMedia, Stack, Typography } from "@mui/material";
import FiberManualRecordIcon from "@mui/icons-material/FiberManualRecord";
import { modificationColors } from "../utils/mappool-utils";
import { useRouter } from "next/router";

const MappoolCard = (props) => {
  const { mappool, tournament } = props;
  const router = useRouter();

  return (
    <Card
      onClick={() => router.push(`/tournaments/${tournament.id}/mappool/${mappool.stage}`)}
      sx={{
        mb: 5,
        "&:hover": {
          cursor: "pointer",
        },
      }}
    >
      <CardMedia height={10}>
        <Stack direction="row" sx={{ height: 40 }}>
          {mappool.beatmapModifications.map((bm) => {
            return bm.beatmaps.map((beatmap) => (
              <img
                src={beatmap.slimCover}
                alt={beatmap.title}
                key={beatmap.id}
                style={{
                  width: "100%",
                  height: "auto",
                  opacity: 0.4,
                  margin: 1,
                }}
              />
            ));
          })}
        </Stack>
      </CardMedia>
      <CardContent>
        <Box
          sx={{
            display: "flex",
            justifyContent: "space-between",
            alignItems: "center",
            flexWrap: "wrap",
          }}
        >
          <Typography color="textPrimary" gutterBottom variant="h4">
            {mappool.stage}
          </Typography>
          <Typography color="textPrimary" gutterBottom variant="body2">
            {mappool.beatmapModifications.map((bm) => {
              if (bm.hidden) return undefined;
              return (
                <>
                  <FiberManualRecordIcon
                    sx={{ fontSize: 10, mr: 1, ml: 2, color: modificationColors(bm.modification) }}
                  />
                  {bm.modification}: {bm.beatmaps.length}
                </>
              );
            })}
          </Typography>
        </Box>
      </CardContent>
    </Card>
  );
};

export default MappoolCard;
